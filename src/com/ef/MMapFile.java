/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ef;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.util.Iterator;
import java.util.NoSuchElementException;

public class MMapFile {

    public class MMapIterator implements Iterator<String> {

        private int offset;

        public MMapIterator(int offset) {
            this.offset = offset;
        }

        @Override
        public boolean hasNext() {
            return offset < buffer.limit();
        }

        @Override
        public String next() {
            ByteArrayOutputStream sb = new ByteArrayOutputStream();
            if (offset >= buffer.limit()) {
                throw new NoSuchElementException();
            }
            for (; offset < buffer.limit(); offset++) {
                byte c = (buffer.get(offset));
                if (c == '\n') {
                    offset++;
                    break;
                }
                if (c != '\r') {
                    sb.write(c);
                }

            }
            try {
                return sb.toString("UTF-8");
            } catch (UnsupportedEncodingException e) {
            }
            return sb.toString();
        }

        @Override
        public void remove() {

        }
    }

    private final MappedByteBuffer buffer;
    long size;
    private long numLines = -1;

    public MMapFile(String file) throws FileNotFoundException, IOException {
        FileChannel fc = new FileInputStream(new File(file)).getChannel();
        size = fc.size();
        buffer = fc.map(FileChannel.MapMode.READ_ONLY, 0, fc.size());
    }

    public long getNumLines() {
        if (numLines != -1) {
            return numLines;  //cache number of lines
        }
        long cnt = 0;
        for (int i = 0; i < size; i++) {
            if (buffer.get(i) == '\n') {
                cnt++;
            }
        }
        numLines = cnt;
        return cnt;
    }

    public Iterator<String> tail(long lines) {
        long cnt = 0;
        long i = 0;
        for (i = size - 1; i >= 0; i--) {
            if (buffer.get((int) i) == '\n') {
                cnt++;
                if (cnt == lines + 1) {
                    break;
                }
            }
        }
        return new MMapIterator((int) i + 1);
    }

    public Iterator<String> head() {
        return new MMapIterator(0);
    }

}
