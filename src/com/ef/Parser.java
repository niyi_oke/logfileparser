/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ef;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import java.util.function.Function;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author NiyiO
 */
public class Parser {
//java -cp "parser.jar" com.ef.Parser --accesslog=/path/to/file --startDate=2017-01-01.13:00:00 --duration=hourly --threshold=100 

//    java -cp "parser.jar" com.ef.Parser --accesslog=C:\Users\NiyiO\Documents\Java_MySQL_Test\access.log --startDate=2017-01-01.13:00:00 --duration=hourly --threshold=100
    private static final String MYSQLURL = "jdbc:mysql://localhost:3306";

    private static final String DATABASESCHEMA = "accesslogdb";

    private static final String USERNAME = "root";

    private static final String PASSWORD = "rememberme2";

    private static final Logger LOGGER = Logger.getLogger(Parser.class.getName());

    public static void main(String... args) throws Exception {

        String accesslog = args[0].substring(12, args[0].length());
        String startDate = args[1].substring(12, args[1].length());
        String duration = args[2].substring(11, args[2].length());
        String threshold = args[3].substring(12, args[3].length());

        validateArguments(accesslog, startDate, duration, threshold);
        setupDB();
        saveLogFileToDB(accesslog);
        findSuspiciousIPs(startDate.replace(".", " "), duration, Integer.parseInt(threshold));

    }

    private static void validateArguments(String accesslog, String startDate, String duration, String threshold) throws Exception {
        if (Objects.isNull(accesslog) || accesslog.isEmpty()) {
            throw new Exception("Invalid accesslog value");
        }
        if (Objects.isNull(startDate) || startDate.isEmpty()) {
            throw new Exception("Invalid startDate value");
        }
        if (Objects.isNull(duration) || duration.isEmpty()) {
            throw new Exception("Invalid duration value");
        }
        if (Objects.isNull(threshold) || threshold.isEmpty()) {
            throw new Exception("Invalid threshold value");
        }
    }

    private static void setupDB() {
        Connection connection = null;
        try {
            connection = DriverManager.getConnection(MYSQLURL, USERNAME, PASSWORD);
            ResultSet schemas = connection.getMetaData().getCatalogs();
            List<String> schemasNames = new ArrayList<>();
            while (schemas.next()) {
                String schemaName = schemas.getString("TABLE_CAT");
                schemasNames.add(schemaName);
            }

            if (schemasNames.isEmpty() || !schemasNames.contains(DATABASESCHEMA)) {
                Statement statement = connection.createStatement();
                statement.execute("CREATE DATABASE " + DATABASESCHEMA);
            }

            connection = DriverManager.getConnection(MYSQLURL + "/" + DATABASESCHEMA + "?zeroDateTimeBehavior=convertToNull", USERNAME, PASSWORD);
            ResultSet tables = connection.getMetaData().getTables(null, null, null, new String[]{"TABLE"});
            List<String> tableNames = new ArrayList<>();
            while (tables.next()) {
                String tableName = tables.getString("TABLE_NAME");
                tableNames.add(tableName);
            }

            Statement statement = connection.createStatement();
            //            if table does not exist create it
            if (tableNames.isEmpty() || !tableNames.contains("accesslog")) {

                statement.execute("CREATE TABLE AccessLog ("
                        + "    Id BIGINT(20) NOT NULL AUTO_INCREMENT ,"
                        + "    DateTime varchar(30),"
                        + "    IpAddress varchar(30),"
                        + "    HttpVerb varchar(255),"
                        + "    StatusCode varchar(5),"
                        + "    ClientDevice varchar(255),"
                        + "    PRIMARY KEY (`Id`) "
                        + ")");
            }
            if (tableNames.isEmpty() || !tableNames.contains("suspiciousips")) {
                statement.execute("CREATE TABLE SuspiciousIPs ("
                        + "    Id BIGINT(20) NOT NULL AUTO_INCREMENT ,"
                        + "    IpAddress varchar(30),"
                        + "    Comment varchar(255),"
                        + "    PRIMARY KEY (`Id`) "
                        + ")");

            }
        } catch (SQLException ex) {
            LOGGER.log(Level.SEVERE, null, ex);
        }
    }

    private static void saveLogFileToDB(String filePath) {

        Connection connection = null;
        PreparedStatement prepareStatement = null;
        PreparedStatement prepareStatement1 = null;
        try {
            connection = DriverManager.getConnection(MYSQLURL + "/" + DATABASESCHEMA + "?zeroDateTimeBehavior=convertToNull", USERNAME, PASSWORD);

            prepareStatement = connection.prepareStatement("INSERT INTO AccessLog (DateTime, IpAddress, HttpVerb, StatusCode, ClientDevice) VALUES (?, ?, ?, ?,?)");

            prepareStatement1 = connection.prepareStatement("SELECT Id FROM accesslogdb.accesslog ORDER BY Id DESC LIMIT 1");

        } catch (SQLException ex) {
            LOGGER.log(Level.SEVERE, null, ex);
        }

        //check if all the log file has been loaded to DB.
        ResultSet resultSet;
        List<Long> Ids = new ArrayList<>();
        try {
            resultSet = prepareStatement1.executeQuery();

            while (resultSet.next()) {
                long aLong = resultSet.getLong("Id");
                Ids.add(aLong);
            }
        } catch (SQLException ex) {
            LOGGER.log(Level.SEVERE, null, ex);
        }

        Long Id = Ids.get(0);

        MMapFile mMapFile = null;
        try {
            mMapFile = new MMapFile(filePath);
        } catch (IOException ex) {
            LOGGER.log(Level.SEVERE, null, ex);
        }

        Iterator<String> head = mMapFile.head();
        long numLines = mMapFile.getNumLines();
        long yetToBeWrittenLines = numLines - Id;

        //write all lines to DB
        if (yetToBeWrittenLines == 0) {

            while (head.hasNext()) {

                String singleLine = head.next();

                String[] items = singleLine.split("\\|");

                try {
                    prepareStatement.setString(1, items[0]);
                    prepareStatement.setString(2, items[1]);
                    prepareStatement.setString(3, items[2]);
                    prepareStatement.setString(4, items[3]);
                    prepareStatement.setString(5, items[4]);
                    prepareStatement.execute();

                } catch (SQLException ex) {
                    LOGGER.log(Level.SEVERE, null, ex);
                }
            }
        } else if (yetToBeWrittenLines > 0) {//write yetToBeWrittenLines to DB
            Iterator<String> tail = mMapFile.tail(yetToBeWrittenLines);
            while (tail.hasNext()) {

                String singleLine = tail.next();

                String[] items = singleLine.split("\\|");

                try {
                    prepareStatement.setString(1, items[0]);
                    prepareStatement.setString(2, items[1]);
                    prepareStatement.setString(3, items[2]);
                    prepareStatement.setString(4, items[3]);
                    prepareStatement.setString(5, items[4]);
                    prepareStatement.execute();

                } catch (SQLException ex) {
                    LOGGER.log(Level.SEVERE, null, ex);
                }
            }
//        }else{
//            assumes file is already written to DB
//        }
        }
    }

    private static void findSuspiciousIPs(String startDateTime, String duration, int threshold) {

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = new Date();
        String endDateTime = "";
        try {
            date = sdf.parse(startDateTime);
            Instant instant = date.toInstant();
            Duration frequency = Enum.valueOf(Duration.class, duration.toUpperCase());
            switch (frequency) {
                case DAILY:
                    instant = instant.plus(1L, ChronoUnit.DAYS).minusSeconds(1);
                    endDateTime = sdf.format(instant.toEpochMilli());
                    threshold = threshold <= 0 ? threshold = 500 : threshold;
                    queryForSuspiciousIPs(startDateTime, endDateTime, threshold);
                    break;
                case HOURLY:
                    instant = instant.plus(1L, ChronoUnit.HOURS).minusSeconds(1);
                    endDateTime = sdf.format(instant.toEpochMilli());
                    threshold = threshold <= 0 ? threshold = 200 : threshold;
                    queryForSuspiciousIPs(startDateTime, endDateTime, threshold);
                    break;
                default:
                    break;
            }

        } catch (ParseException ex) {
            LOGGER.log(Level.SEVERE, null, ex);
        }

    }

    private static void queryForSuspiciousIPs(String startDateTime, String endDateTime, int threshold) {

        Connection connection = null;
        PreparedStatement prepareStatement = null;
        ResultSet resultSet = null;
        List<String> ips = new ArrayList<>();
        try {
            connection = DriverManager.getConnection(MYSQLURL + "/" + DATABASESCHEMA + "?zeroDateTimeBehavior=convertToNull", USERNAME, PASSWORD);
            prepareStatement = connection.prepareStatement("SELECT accesslog.IpAddress FROM AccessLog WHERE accesslog.DateTime >=? AND accesslog.DateTime <=? GROUP BY IpAddress HAVING COUNT(accesslog.IpAddress) > ?");
            prepareStatement.setString(1, startDateTime);
            prepareStatement.setString(2, endDateTime);
            prepareStatement.setInt(3, threshold);
            resultSet = prepareStatement.executeQuery();

            while (resultSet.next()) {
                String ipAddress = resultSet.getString("IpAddress");
                ips.add(ipAddress);
            }
        } catch (SQLException ex) {
            LOGGER.log(Level.SEVERE, null, ex);
        }

        saveSuspicousIps(ips, startDateTime, endDateTime, threshold);

    }

    private static void saveSuspicousIps(List<String> ips, String startDateTime, String endDateTime, int threshold) {
        Connection connection = null;
        PreparedStatement prepareStatement = null;
        try {
            connection = DriverManager.getConnection(MYSQLURL + "/" + DATABASESCHEMA + "?zeroDateTimeBehavior=convertToNull", USERNAME, PASSWORD);
            prepareStatement = connection.prepareStatement("INSERT INTO suspiciousips (IpAddress,Comment) VALUES (?,?)");

            for (int i = 0; i < ips.size(); i++) {
                String ipAddress = ips.get(i);
                String comment = ipAddress + ". If you open the log file, " + ipAddress + " has " + threshold + " or more requests between " + startDateTime.replace(" ", ".") + " and " + endDateTime.replace(" ", ".");
                System.out.println(comment);
                prepareStatement.setString(1, ipAddress);
                prepareStatement.setString(2, comment);
                prepareStatement.execute();
            }

        } catch (SQLException ex) {
            LOGGER.log(Level.SEVERE, null, ex);
        }

    }

}
